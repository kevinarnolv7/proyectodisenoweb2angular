/*Implemento 2: Paso 3: Se crea el routing.module.ts con el comando ng generate module AgradecimientoCompra-routing --flat --module=app
en el /src/pages/agradecimiento-compra
 para poder ir a este archivo con un RouterLink y se importa el component
*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgradecimientoCompraComponent } from './agradecimiento-compra.component';
//las contaste de la ruta, que son variables que no se pueden editar
const routes: Routes = [{
  path: '',
  title: 'Agradecimiento-DeTu compra',
  component:  AgradecimientoCompraComponent
}];
//Implemento 2: Paso 6: se añade el import y exports del routermodule.forchild
//actua como hijos de rutas
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AgradecimientoCompraRoutingModule { }
