//Implemento 2: Paso 4: Se crea el module.ts con el comando ng generate module AgradecimientoCompra

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Implemento 2: Paso 5: Se Importa el RoutingModule y el Component
import { AgradecimientoCompraRoutingModule } from './agradecimiento-compra-routing.module';
import { AgradecimientoCompraComponent } from './agradecimiento-compra.component';

@NgModule({
  declarations: [AgradecimientoCompraComponent],
  imports: [
    CommonModule,
    AgradecimientoCompraRoutingModule
  ]
})
export class AgradecimientoCompraModule { }
