//Implemento 4: Paso 4: Se crea el module.ts con el comando ng generate module MensajeContacto
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Implemento 4: Paso 5: Se Importa el RoutingModule y el Component
import { MensajeContactoRoutingModule} from './mensaje-contacto-routing.module';
import {MensajeContactoComponents } from './mensaje-contacto.component';



@NgModule({
  declarations: [MensajeContactoComponents],
  imports: [
    CommonModule, 
    MensajeContactoRoutingModule
  ]
})
export class MensajeContactoModule { }
