//Implemento 3: Paso 4: Se crea el module.ts con el comando ng generate module Contacto

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/*Implemento 3: Paso 5: Se Importa el RoutingModule, el component y el formsmodule y reactiveformsmodule
para intentar hacer la validacion pero no me dio :c*/
import { ContactoRoutingModule } from './contacto-routing.module';
import {ContactoComponent} from './contacto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ContactoComponent],
  imports: [
    CommonModule,
    ContactoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ContactoModule { }
