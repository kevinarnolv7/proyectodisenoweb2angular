/*Implemento 1: Paso 3: Cree el routing.module.ts con el comando ng generate module RealizarCompra-routing --flat --module=app
en el /src/pages/realizar-compra
 para poder ir a este archivo con un RouterLink y se importa el component
*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RealizarCompraComponent } from './realizar-compra.component';
//contaste que son variables que no se puede modificar
const routes: Routes = [{
  path: '',
  title: 'Realiza-Tu compra',
  component:  RealizarCompraComponent
}];
//Implemento 1: Paso 6: se añade el import y exports del routermodule.forchild
//actua como hijos de rutas
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RealizarCompraRoutingModule { }
