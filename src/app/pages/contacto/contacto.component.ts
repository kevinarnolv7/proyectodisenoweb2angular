import { Component } from '@angular/core';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
/*Implemento 3: Paso 3: Intento de validacion en el formulario, no me salio :c */
export class ContactoComponent {
  nombre: string = '';
  telefono: string = '';
  correo: string = '';
  nombreError: string = '';
  telefonoError: string = '';
  correoError: string = '';

  submitForm() {
    this.nombreError = '';
    this.telefonoError = '';
    this.correoError = '';

    if (this.isValid()) {
      console.log('Formulario válido. Puedes realizar acciones aquí.');
    } else {
      console.error('Formulario no válido. Revise los campos.');
    }
  }

  isValid(): boolean {
    let valid = true;

    if (this.nombre.trim() === '') {
      this.nombreError = 'El nombre es obligatorio.';
      valid = false;
    }

    if (this.telefono.trim() === '') {
      this.telefonoError = 'El teléfono es obligatorio.';
      valid = false;
    }

    if (!this.isEmailValid(this.correo)) {
      this.correoError = 'El correo electrónico no es válido.';
      valid = false;
    }

    return valid;
  }

  isEmailValid(email: string): boolean {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
  }
}
