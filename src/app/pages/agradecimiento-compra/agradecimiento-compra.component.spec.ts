import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgradecimientoCompraComponent } from './agradecimiento-compra.component';

describe('AgradecimientoCompraComponent', () => {
  let component: AgradecimientoCompraComponent;
  let fixture: ComponentFixture<AgradecimientoCompraComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AgradecimientoCompraComponent]
    });
    fixture = TestBed.createComponent(AgradecimientoCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
