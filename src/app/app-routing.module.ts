import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProductosComponent } from './components/productos/productos.component';
import { StoreComponent } from './pages/store/store.component';
import { FallaComponent } from './pages/falla/falla.component';
import { estaloginGuard } from './guard/estalogin.guard';
import { nologinGuard } from './guard/nologin.guard';
import { DetalleproductComponent } from './pages/detalleproduct/detalleproduct.component';

const routes: Routes = [
  {
    path: '',
    title: 'Home-Tu tienda',
    component: HomeComponent
  },
  {
    path: 'login',
    canActivate: [nologinGuard],
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'registro',
    canActivate: [nologinGuard],
    loadChildren: () => import('./pages/registro/registro.module').then(m => m.RegistroModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule)
  },
  /*Implemento 1: Paso 8: se crea el
     {
    path: 'realizar-compra',
    loadChildren: () => import('./pages/realizar-compra/realizar-compra.module').then(m => m.RealizarCompraModule)
  },
   para que pueda funcionar el routerlink en path va el nombre del archivo se carga los niños y se importa
   de la direccion de compra.module.*/
  {
    path: 'realizar-compra',
    loadChildren: () => import('./pages/realizar-compra/realizar-compra.module').then(m => m.RealizarCompraModule)
  },
  /*Implemento 2: Paso 8: se crea el
    {
    path: 'agradecimiento-compra',
    loadChildren: () => import('./pages/agradecimiento-compra/agradecimiento-compra.module').then(m => m.AgradecimientoCompraModule)
  },
      para que pueda funcionar el routerlink en path va el nombre del archivo se carga los niños y se importa
   de la direccion de compra.module.*/ 
  {
    path: 'agradecimiento-compra',
    loadChildren: () => import('./pages/agradecimiento-compra/agradecimiento-compra.module').then(m => m.AgradecimientoCompraModule)
  },
  /*Implemento 3: Paso 8: se crea el
      path: 'contacto',
    loadChildren: () => import('./pages/contacto/contacto.module').then(m => m.ContactoModule)
      para que pueda funcionar el routerlink en path va el nombre del archivo se carga los niños y se importa
   de la direccion de compra.module.*/ 
  {
    path: 'contacto',
    loadChildren: () => import('./pages/contacto/contacto.module').then(m => m.ContactoModule)
  },
  /*Implemento 4: Paso 8: se crea el
   {
    path: 'mensaje-contacto',
    loadChildren: () => import('./pages/mensaje-contacto/mensaje-contacto.module').then(m => m.MensajeContactoModule)
  },
           para que pueda funcionar el routerlink en path va el nombre del archivo se carga los niños y se importa
   de la direccion de compra.module.*/
  {
    path: 'mensaje-contacto',
    loadChildren: () => import('./pages/mensaje-contacto/mensaje-contacto.module').then(m => m.MensajeContactoModule)
  },
  {
    path: 'store',
    title: 'productos-Tu tienda',
    component: StoreComponent
  },
  {
    path: 'store/:detalle',
    title: 'productos-Tu tienda',
    component: DetalleproductComponent
  },
  {
    path: '**',
    title: '404-Tu tienda',
    component: FallaComponent
  }
  
];
//actua como proovedor de las rutas
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  
  
})
export class AppRoutingModule { }
