//Implemento 1: Paso 4: Se crea el module.ts con el comando ng generate module RealizarCompra
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Implemento 1: Paso 5: Se Importa el RoutingModule y el Component
import { RealizarCompraRoutingModule } from './realizar-compra-routing.module';
import { RealizarCompraComponent } from './realizar-compra.component';


@NgModule({
  declarations: [RealizarCompraComponent],
  imports: [
    CommonModule,
    RealizarCompraRoutingModule,
  ]
})
export class RealizarCompraModule { }
