import { Component, OnInit } from '@angular/core';
//Implemento 1: Paso 9: Se exporta el CartService donde estan los datos de los productos
import { CartService } from 'src/app/service/cart.service';
@Component({
  selector: 'app-realizar-compra',
  templateUrl: './realizar-compra.component.html',
  styleUrls: ['./realizar-compra.component.css']
})
export class RealizarCompraComponent   {
  //Implemento 1: Paso 10: Se construye el cartservice con la variable cart para poder usarla
  constructor(  private cart: CartService) { }
  //Implemento 1: Paso 11: Se crea la funcion allremove para remove los productos una vez comprados
  allRemove() {
    this.cart.removeAll()
  }
}
