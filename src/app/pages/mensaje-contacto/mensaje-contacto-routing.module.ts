/*Implemento 1: Paso 3: Cree el routing.module.ts con el comando ng generate module MensajeContacto-routing --flat --module=app
en el /src/pages/mensaje-contacto
 para poder ir a este archivo con un RouterLink y se importa el component
*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MensajeContactoComponents } from './mensaje-contacto.component';



const routes: Routes = [{
  path: '',
  title: 'Mensaje-de contacto',
  component:  MensajeContactoComponents
}];
//Implemento 4: Paso 6: se añade el import y exports del routermodule.forchild
//actua como hijos de rutas
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MensajeContactoRoutingModule { }
