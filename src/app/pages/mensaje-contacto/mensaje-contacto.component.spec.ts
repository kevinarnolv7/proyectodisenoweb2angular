import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeContactoComponents } from './mensaje-contacto.component';

describe('MensajeContactoComponent', () => {
  let component: MensajeContactoComponents;
  let fixture: ComponentFixture<MensajeContactoComponents>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MensajeContactoComponents]
    });
    fixture = TestBed.createComponent(MensajeContactoComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
