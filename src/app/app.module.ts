import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { AllComponent } from './components/all/all.component';
import { StoreComponent } from './pages/store/store.component';
// import { LoginComponent } from './pages/login/login.component';
import { CartComponent } from './components/cart/cart.component';
import { CarrouserComponent } from './components/carrouser/carrouser.component';
import { ProductosComponent } from './components/productos/productos.component';
import { HttpClientModule } from '@angular/common/http';
// import { RegistroComponent } from './pages/registro/registro.component';
import { SegundoComponent } from './components/segundo/segundo.component';
import { TerceroComponent } from './components/tercero/tercero.component';
import { CuartoComponent } from './components/cuarto/cuarto.component';
import { GarantiasComponent } from './components/garantias/garantias.component';
import { FallaComponent } from './pages/falla/falla.component';
import { DetalleproductComponent } from './pages/detalleproduct/detalleproduct.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoaderComponent } from './components/loader/loader.component';
import { CheckComponent } from './components/check/check.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/*Implemento 4: Paso 7: Se borra las importaciones del componente y el routermodule del archivo MensajeContacto
y del NgModule y de los imports de la parte de abajo*/
//import { MensajeContactoComponents } from './pages/mensaje-contacto/mensaje-contacto.component';
/*Implemento 2: Paso 7: Se borra las importaciones del componente y el routermodule del archivo AgradecimientoCompra
y del NgModule y de los imports de la parte de abajo*/
//import { AgradecimientoCompraComponent } from './pages/agradecimiento-compra/agradecimiento-compra.component';
/*Implemento 3: Paso 7: Se borra las importaciones del componente y el routermodule del archivo Contacto
y del NgModule y de los imports de la parte de abajo*/
//import { ContactoComponent } from './pages/contacto/contacto.component';
/*Implemento 1: Paso 7: Se borra las importaciones del componente y el routermodule del archivo RealizarCompra 
y del NgModule y de los imports de la parte de abajo*/
//import { RealizarCompraComponent } from './pages/realizar-compra/realizar-compra.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    AllComponent,
    StoreComponent,
    // LoginComponent,
    CartComponent,
    CarrouserComponent,
    ProductosComponent,
    // RegistroComponent,
    SegundoComponent,
    TerceroComponent,
    CuartoComponent,
    GarantiasComponent,
    FallaComponent,
    DetalleproductComponent,
    FooterComponent,
    LoaderComponent,
    CheckComponent,
    //MensajeContactoComponents,
    //ContactoComponent,
    //AgradecimientoCompraComponent,
    //RealizarCompraComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
